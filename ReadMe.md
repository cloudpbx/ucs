
## How to Use
- Replace region-[am].cfg to current region from 000000000000.cfg
- Place region specific config to most left hand side to prevent overwrite or unable to overwrite issue
-   e.g. region-am.cfg,model-[PHONE_MODEL].cfg,shared.cfg 
- change ntp related parameters in your region-[name].cfg
- replace <ct>*sip:+1418889@deltaww.com*</ct> in contacts/000000000000-directory.xml
- please be aware 000000000000.cfg already skipped from index, by "git update-index --skip-worktree 000000000000.cfg"

### Supported regions
- region-am.cfg for whole North America region
- region-cn-s.cfg for China South
- region-latam.cfg for LATAM
- region-nea.cfg for NEA
- region-tw.cfg for Taiwan

### To DO
seperate config for time server and time zone?

### Changelog
10/14/2019 - add local contact in 00000000-directory.xml under contacts folder for Trio
 

### Current version
- vvx is 6.1.0.6189 ( last release for VVX 300/310/250/601 )
- trio is 5.9.0.11421

### Part number
- SoundStructure VoIP Interface 3111-33215-001 (Model is SSTRVOIP)
- VVX 311 is 3111-48350-001
- VVX 250 is 3111-48820-001
- VVX 601 is 3111-48600-001
- Trio 8800 is 3111-65290-001
- Visual+ is 3111-66420-001
- [list of part numbers!](https://community.polycom.com/t5/VoIP-SIP-Phones/FAQ-Can-I-use-substitutions-when-provisioning-my-Phones/td-p/4249)
